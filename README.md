# WP Core

Installs WordPress to the virtual machine and optionally installs a
WordPress Core.

## Notes

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL
NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and
"OPTIONAL" in this document are to be interpreted as described in
RFC 2119.

## Sample Usage

    box: wercker/php
    deploy:
      steps:
        - uk-hando/wp-core:
          cwd: $WERCKER_ROOT/public/
          core-version: 4.8.1
          generate-salts: true

&nbsp;

## Step Properties

### core-version (required)

The version of WordPress you would like to download.

* Since: `0.0.1`
* Property is `Required`
* Recommendation location: `Inline`
* `Validation` rules:
  * Must be a valid [WordPress version](https://codex.wordpress.org/WordPress_Versions)

&nbsp;

### generate-salts

Whether or not to download fresh salts to the `wp-salts.php` file.

* Since: `0.0.6`
* Property is `Optional`
* Recommendation location: `Inline`
* `Validation` rules:
  * Must be either `true`, `false`, `1` or `0`

&nbsp;
